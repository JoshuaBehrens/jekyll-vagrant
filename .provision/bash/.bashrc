alias browser-server='screen -d -m -s browsersync bash -c "cd /home/vagrant/jekyll_page/_site; browser-sync start --config ../.provision/browser-sync/bs-config.js"'
alias jekyll-server='screen -d -m -s jekyll bash -c "cd /home/vagrant/jekyll_page; bundle exec jekyll build --force_polling --watch"'

cd /home/vagrant/jekyll_page
